package com.finzsoft.interview.model;

import java.time.LocalDate;

public class Address {

    private String address;
    private LocalDate effectiveDate;


    public Address() {
    }

    public Address(String address) {
        this.address = address;
    }

    public Address(String address, LocalDate effectiveDate) {
        this.address = address;
        this.effectiveDate = effectiveDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }
}
